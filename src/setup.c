/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * hp-drive-guard client system-setup window
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *                      
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "../config.h"
#include "common.h"
#include "serverconfig.h"

#define progname        DRIVE_GUARD_CLIENT_PROG_NAME

typedef struct {
  DriveGuardServerConfig config;
  GtkWidget *dialog;
  GtkWidget *config_vbox;
  GtkWidget *device_list;
  gboolean allow_user_setup;
} SetupDialog;

static void
device_enable_toggled_cb (GtkToggleButton *button, SetupDialog *setup)
{
  setup->config.enabled = gtk_toggle_button_get_active (button);
  gtk_widget_set_sensitive (GTK_WIDGET (setup->config_vbox),
                            setup->config.enabled);
}

static void
int_value_changed_cb (GtkSpinButton *button, int *value_ret)
{
  *value_ret = gtk_spin_button_get_value (button);
}

static void
create_int_entry (GtkWidget *table, gint idx, const gchar *str,
                  int *value_ret, int minval, int maxval)
{
  GtkWidget *label;
  GtkWidget *spin;

  label = gtk_label_new (str);
  gtk_widget_show (label);
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, idx, idx + 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  spin = gtk_spin_button_new_with_range (minval, maxval, 1);
  gtk_widget_show (spin);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), *value_ret);
  gtk_table_attach (GTK_TABLE (table), spin, 1, 2, idx, idx + 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  g_signal_connect (spin, "value-changed",
                    G_CALLBACK (int_value_changed_cb), value_ret);
}

static void
call_helper(const char *helper, DriveGuardServerConfig *config)
{
  char *cmd;
  int i;

  cmd = g_strdup_printf ("%s %s/%s -a%d -t%d -e%d -r%d",
                         helper,
                         HELPER_EXEC_DIR, DRIVE_GUARD_SAVE_CONFIG_PROG_NAME,
                         config->enabled,
                         config->default_timeout,
                         config->extended_timeout,
                         config->sched_prio);
  for (i = 0; i < config->num_devices; i++) {
    char *newcmd = g_strconcat (cmd, " ", config->devices [i], NULL);
    g_free (cmd);
    cmd = newcmd;
  }

  if (!g_spawn_command_line_sync (cmd, NULL, NULL, NULL, NULL))
    fprintf (stderr, "%s: cannot execute helper %s/%s via %s\n",
             progname,
             HELPER_EXEC_DIR, DRIVE_GUARD_SAVE_CONFIG_PROG_NAME,
             helper);

  g_free (cmd);
}

static void
convert_to_device_list (SetupDialog *setup)
{
  const gchar *text = gtk_entry_get_text (GTK_ENTRY (setup->device_list));

  if (text) {
    if (setup->config.devices)
      g_strfreev ((gchar **)setup->config.devices);
    setup->config.devices = (const char **)g_strsplit (text, " ", 32);
    setup->config.num_devices = g_strv_length ((gchar **)setup->config.devices);
  }
}

static void
write_setup (SetupDialog *setup)
{
  convert_to_device_list (setup);

  if (!access("/usr/bin/pkexec", X_OK))
    call_helper("pkexec", &setup->config);
  else
    call_helper("gnomesu --", &setup->config);
}

static SetupDialog *_setup;

static void response_cb (GtkWidget *dialog, gint result, gpointer data)
{
  SetupDialog *setup = (SetupDialog *)data;
  if (result == GTK_RESPONSE_ACCEPT)
    write_setup (setup);
  
  gtk_widget_destroy (dialog);
  g_free (setup);
  _setup = NULL;
}

static SetupDialog *
create_setup_dialog (int allow_user_setup)
{
  SetupDialog *setup;
  DriveGuardServerConfig *config;
  GtkWidget *dialog;
  GtkWidget *vbox1;
  GtkWidget *device_enable;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *vbox2;
  GtkWidget *table;
  GtkWidget *device_list;

  setup = g_new0 (SetupDialog, 1);
  setup->allow_user_setup = allow_user_setup;
  config = &setup->config;
  drive_guard_init_config (config);
  drive_guard_read_config (config);

  dialog = gtk_dialog_new_with_buttons (_("HP DriveGuard Setup"), NULL,
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
                                        GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
                                        NULL);
  setup->dialog = dialog;

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox1, FALSE, FALSE, 0);

  device_enable = gtk_check_button_new_with_label (_("Enable HP DriveGuard"));
  gtk_widget_show (device_enable);
  gtk_box_pack_start (GTK_BOX (vbox1), device_enable, FALSE, FALSE, 5);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (device_enable), config->enabled);
  g_signal_connect (device_enable, "toggled",
                    G_CALLBACK (device_enable_toggled_cb), setup);

  vbox2 = gtk_vbox_new (FALSE, 0);
  if (allow_user_setup)
    gtk_widget_show (vbox2);
  else
    gtk_widget_hide (vbox2);
  gtk_box_pack_start (GTK_BOX (vbox1), vbox2, FALSE, FALSE, 0);
  setup->config_vbox = vbox2;
  gtk_widget_set_sensitive (GTK_WIDGET (vbox2), config->enabled);

  table = gtk_table_new (3, 2, FALSE);
  gtk_widget_show (table);
  gtk_box_pack_start (GTK_BOX (vbox2), table, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (table), 10);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);

  create_int_entry (table, 0, _("Default Timeout"),
                    &config->default_timeout, 1, 30);
  create_int_entry (table, 1, _("Extended Timeout"),
                    &config->extended_timeout, 1, 30);
  create_int_entry (table, 2, _("Daemon Task Priority"),
                    &config->sched_prio, 0, 50);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (hbox);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox, TRUE, TRUE, 0);

  label = gtk_label_new (_("Devices"));
  gtk_widget_show (label);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  device_list = gtk_entry_new ();
  setup->device_list = device_list;
  if (config->num_devices > 0) {
    gchar *text = g_strjoinv (" ", (gchar **)config->devices);
    gtk_entry_set_text (GTK_ENTRY (device_list), text);
    g_free (text);
  }
  g_signal_connect (device_enable, "toggled",
                    G_CALLBACK (device_enable_toggled_cb), setup);
  gtk_widget_show (device_list);
  gtk_box_pack_start (GTK_BOX (hbox), device_list, TRUE, TRUE, 0);

  g_signal_connect (setup->dialog, "response", 
                    G_CALLBACK (response_cb), setup);
  return setup;
}

void drive_guard_do_setup (int allow_user_setup)
{
  if (!_setup)
    _setup = create_setup_dialog (allow_user_setup);
  gtk_widget_show (_setup->dialog);
}
