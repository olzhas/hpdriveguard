/*
 * hp-drive-guard common definitions
 *
 *   Copyright (c) 2010 Hans Petter Jansson <hpj@novell.com>
 *                      Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#define DRIVE_GUARD_DBUS_NAME         "com.hp.DriveGuard"
#define DRIVE_GUARD_DBUS_SERVER_NAME  DRIVE_GUARD_DBUS_NAME ".Server"
#define DRIVE_GUARD_DBUS_SERVER_IFACE DRIVE_GUARD_DBUS_SERVER_NAME
#define DRIVE_GUARD_DBUS_SERVER_PATH  "/com/hp/DriveGuard/Server"

typedef enum
{
  DRIVE_GUARD_DRIVE_STATE_NONE,
  DRIVE_GUARD_DRIVE_STATE_UNSUPPORTED,
  DRIVE_GUARD_DRIVE_STATE_SSD_ONLY,
  DRIVE_GUARD_DRIVE_STATE_DISABLED,
  DRIVE_GUARD_DRIVE_STATE_STOPPED,
  DRIVE_GUARD_DRIVE_STATE_ENABLED,
  DRIVE_GUARD_DRIVE_STATE_PARKED,

  DRIVE_GUARD_DRIVE_STATE_MAX
}
DriveState;

#define DRIVE_GUARD_POLICY_ACTION_TOGGLE	"com.hp.driveguard.toggle"

#define DRIVE_GUARD_SHM_PATH		"/hp-drive-guard-daemon-shm"

#define FREEFALL_WATCH_DEVICE   "/dev/freefall"

#define SHOCK_TIMEOUT_S_DEFAULT		2
#define SHOCK_TIMEOUT_S_MAX		30
#define EXTENDED_TIMEOUT_S_DEFAULT	20

#define DRIVE_GUARD_DAEMON_PROG_NAME	"hp-drive-guard-daemon"
#define DRIVE_GUARD_CORE_PROG_NAME	"hp-drive-guard-core"
#define DRIVE_GUARD_CLIENT_PROG_NAME	"hp-drive-guard-client"
#define DRIVE_GUARD_SAVE_CONFIG_PROG_NAME	"hp-drive-guard-save-config"
