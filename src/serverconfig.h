/*
 * hp-drive-guard server
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *                      
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef HDG_SERVERCONFIG_H
#define HDG_SERVERCONFIG_H

#define MAX_DRIVE_GUARD_DEVICES		32

typedef struct _DriveGuardServerConfig {
	int enabled;
	int default_timeout;
	int extended_timeout;
	int sched_prio;
	unsigned int num_devices;
	const char **devices;
} DriveGuardServerConfig;

int drive_guard_init_config(DriveGuardServerConfig *config);
void drive_guard_free_config(DriveGuardServerConfig *config);
int drive_guard_read_config(DriveGuardServerConfig *config);
int drive_guard_write_config(DriveGuardServerConfig *config);

/* in setup.c */
void drive_guard_do_setup (int allow_user_setup);

#endif /* HGD_SERVERCONFIG_H */
