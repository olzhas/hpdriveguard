/*
 * hp-drive-guard daemon backend
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *                      
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef DAEMON_COMM_H_INC
#define DAEMON_COMM_H_INC

#define P_READER	0
#define P_WRITER	1

struct daemon_comm {
	int pipe[2];
	volatile unsigned int serial[2];
	unsigned int val;
};

static inline int daemon_comm_write(struct daemon_comm *comm, unsigned int val)
{
	unsigned int serial = comm->serial[P_WRITER];
	comm->val = val;
	comm->serial[P_WRITER]++;
	if (serial == comm->serial[P_READER]) {
		if (write(comm->pipe[P_WRITER], &val, sizeof(val)) != sizeof(val))
			return 0;
	}
	return 1;
}

static inline int daemon_comm_read(struct daemon_comm *comm, int do_read,
				   unsigned int *val)
{
	if (do_read && read(comm->pipe[P_READER], val, sizeof(*val)) < 0)
		return 0;
	for (;;) {
		unsigned int serial = comm->serial[P_WRITER];
		*val = comm->val;
		if (comm->serial[P_READER] == serial)
			break;
		comm->serial[P_READER] = serial;
	}
	return 1;
}

struct hp_drive_guard_shm {
	struct daemon_comm state;
	struct daemon_comm cmd;
	int freefall_fd;
};

#endif /* DAEMON_COMM_H_INC */
