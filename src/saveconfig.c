/*
 * hp-drive-guard helper binary to save the system-wide config file
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *                      
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../config.h"
#include "common.h"
#include "serverconfig.h"

#define progname	DRIVE_GUARD_SAVE_CONFIG_PROG_NAME

int main(int argc, char **argv)
{
	DriveGuardServerConfig config;
	char *data;
	int len;
	int c;

	drive_guard_init_config(&config);
	drive_guard_read_config(&config);
	while ((c = getopt(argc, argv, "a:t:e:r:")) != -1) {
		switch (c) {
		case 'a':
			config.enabled = atoi(optarg);
			break;
		case 't':
			config.default_timeout = atoi(optarg);
			break;
		case 'e':
			config.extended_timeout = atoi(optarg);
			break;
		case 'r':
			config.sched_prio = atoi(optarg);
			break;
		default:
			fprintf(stderr, "%s: invalid option '%c'\n",
				progname, c);
			return 1;
		}
	}
	if (optind < argc) {
		config.num_devices = argc - optind;
		config.devices = (const char **)(argv + optind);
	}
	drive_guard_write_config(&config);

	/* notify to daemon */
	system("dbus-send --type=method_call --system"
	       " --dest=" DRIVE_GUARD_DBUS_NAME
	       " " DRIVE_GUARD_DBUS_SERVER_PATH
	       " " DRIVE_GUARD_DBUS_SERVER_IFACE ".Reset");

	return 0;
}
