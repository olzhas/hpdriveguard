/*
 * hp-drive-guard daemon
 * 
 * AC-cable and LID status check via HAL
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <dbus/dbus.h>
#include <hal/libhal.h>
#include <glib.h>

#include "laptop-status.h"

struct LaptopHW {
	DBusConnection *dbus;
	LibHalContext *hal_ctx;
	char *ac_adapter;		/* udi */
	char *lid_button;		/* udi */
	struct LaptopHWStatus status;
	LaptopHWCallback callback;
	void *user_data;
};

static gboolean
get_hal_property_bool(LibHalContext *hal, const char *udi, const char *prop)
{
	DBusError error;
	gboolean result;

	dbus_error_init(&error);
	result = libhal_device_get_property_bool(hal, udi, prop, &error);
	if (dbus_error_is_set(&error))
		dbus_error_free(&error);
	return result;
}

static gboolean is_lid_open(LaptopHW *hw)
{
	return !get_hal_property_bool(hw->hal_ctx, hw->lid_button,
				      "button.state.value");
}

static gboolean is_on_ac(LaptopHW *hw)
{
	return get_hal_property_bool(hw->hal_ctx, hw->ac_adapter,
				     "ac_adapter.present");
}

void laptop_hw_status_update(LaptopHW *hw)
{
	if (hw->callback)
		hw->callback(hw, &hw->status, hw->user_data); 
}

static void
hal_devprop_modified_cb(LibHalContext *ctx, const char *udi, const char *key,
			dbus_bool_t is_removed, dbus_bool_t is_added)
{
	LaptopHW *hw = libhal_ctx_get_user_data(ctx);

	if (strncmp(key, "button", 6) == 0) {
		hw->status.is_lid_open = is_lid_open(hw);
		laptop_hw_status_update(hw);
	} else if (strcmp (key, "ac_adapter.present") == 0) {
		hw->status.is_on_ac = is_on_ac(hw);
		laptop_hw_status_update(hw);
	}
}

LaptopHW *
laptop_hw_status_new(LaptopHWCallback callback, void *user_data)
{
	DBusConnection *dbus;
	LaptopHW *hw;
	LibHalContext *hal;
	char **device_names = NULL;
	int i, num_devices;
	DBusError error;

	dbus = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
	dbus_error_init(&error);
	if (dbus_error_is_set(&error)) {
		dbus_error_free(&error);
		return NULL;
	}
	dbus_connection_set_exit_on_disconnect(dbus, FALSE);

	hal = libhal_ctx_new();
	if (!hal) {
		dbus_connection_unref(dbus);
		return NULL;
	}

	hw = g_new0(LaptopHW, 1);
	hw->hal_ctx = hal;
	hw->dbus = dbus;

	libhal_ctx_set_user_data(hal, hw);
	libhal_ctx_set_dbus_connection(hal, dbus);

	dbus_error_init(&error);
	if (!libhal_ctx_init(hal, &error)) {
		if (dbus_error_is_set (&error))
			dbus_error_free (&error);
		g_free(hw);
		dbus_connection_unref(dbus);
		return NULL;
	}

	/* get udi of ac adapter */
	device_names = libhal_find_device_by_capability(hal, "ac_adapter", &num_devices, &error);
	if (!device_names || !device_names[0])
		goto no_hw;

	hw->ac_adapter = strdup(device_names[0]);
	if (!hw->ac_adapter)
		goto no_hw;
	libhal_free_string_array(device_names);

	/* get udi of lid-button */
	device_names = libhal_find_device_by_capability(hal, "button", &num_devices, &error);
	if (!device_names || !device_names[0])
		goto no_hw;

	for (i = 0; i < num_devices; i++) {
		char *type;
		type = libhal_device_get_property_string(hal, device_names[i],
					     "button.type", &error);
		if (!type) {
			if (dbus_error_is_set (&error)) {
				dbus_error_free (&error);
				dbus_error_init(&error);
			}
			continue;
		}
		if (!strcmp(type, "lid")) {
			libhal_free_string(type);
			hw->lid_button = strdup(device_names[i]);
			break;
		}
		libhal_free_string(type);
	}
	if (!hw->lid_button)
		goto no_hw;
	libhal_free_string_array(device_names);

	if (!libhal_device_add_property_watch(hal, hw->ac_adapter, &error))
		goto no_hw;
	if (!libhal_device_add_property_watch(hal, hw->lid_button, &error))
		goto no_hw;
	if (!libhal_ctx_set_device_property_modified(hal, hal_devprop_modified_cb))
		goto no_hw;
	hw->status.is_lid_open = is_lid_open(hw);
	hw->status.is_on_ac = is_on_ac(hw);
	hw->user_data = user_data;
	hw->callback = callback;
	return hw;

 no_hw:
	if (dbus_error_is_set (&error))
		dbus_error_free (&error);
	if (device_names)
		libhal_free_string_array(device_names);
	libhal_ctx_free(hal);
	g_free(hw);
	dbus_connection_unref(dbus);
	return NULL;
}

void laptop_hw_status_free(LaptopHW *hw)
{
	DBusError error;

	if (!hw)
		return;

	dbus_error_init(&error);
	libhal_ctx_shutdown(hw->hal_ctx, &error);
	libhal_ctx_free(hw->hal_ctx);
	dbus_connection_unref(hw->dbus);
	free(hw->ac_adapter);
	free(hw->lid_button);
	g_free(hw);
}

DBusConnection *laptop_hw_get_dbus_connection(LaptopHW *hw)
{
	return hw->dbus;
}
