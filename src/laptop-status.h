/*
 */

#ifndef LAPTOP_STATUS_H_INC
#define LAPTOP_STATUS_H_INC

typedef struct LaptopHW LaptopHW;

typedef struct LaptopHWStatus {
	int is_lid_open;
	int is_on_ac;
} LaptopHWStatus;

typedef void (*LaptopHWCallback)(LaptopHW *, LaptopHWStatus *status, void *userdata);

LaptopHW *laptop_hw_status_new(LaptopHWCallback callback, void *user_data);
void laptop_hw_status_free(LaptopHW *hw);
DBusConnection *laptop_hw_get_dbus_connection(LaptopHW *hw);

void laptop_hw_status_update(LaptopHW *hw);

#endif /* LAPTOP_STATUS_H_INC */
