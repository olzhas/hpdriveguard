/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * hp-drive-guard daemon
 *
 *   Copyright (c) 2010 Hans Petter Jansson <hpj@novell.com>
 *                      Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <signal.h>
#include <poll.h>

#include <glib.h>

#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include <polkit/polkit.h>

#include "common.h"
#include "serverconfig.h"
#include "daemon-comm.h"
#include "laptop-status.h"

#define DBUS_RETRY_INTERVAL_S 10

typedef struct _DriveGuardServer DriveGuardServer;

#define progname        DRIVE_GUARD_DAEMON_PROG_NAME

/* --- Backend --- */

#define CORE_EXEC_PATH	HELPER_EXEC_DIR "/" DRIVE_GUARD_CORE_PROG_NAME

/* check whether the given device name ("sda", etc) is an SSD */
static gboolean is_ssd(const gchar *name)
{
  gchar path[64], result;
  gint fd;

  snprintf(path, sizeof(path), "/sys/block/%s/queue/rotational", name);
  fd = open(path, O_RDONLY);
  if (fd < 0)
    return FALSE;
  result = 0;
  read(fd, &result, 1);
  close(fd);
  return result == '0';
}

/* check the default state of the given device ("sda", etc);
 * returns either SSD_ONLY, NONE, UNSUPPORTED, or ENABLED
 */
static DriveState check_device_state (const gchar *name)
{
  gchar path[128], tmp[32];
  gint read_len;
  gint fd;

  if (is_ssd(name))
    return DRIVE_GUARD_DRIVE_STATE_SSD_ONLY;

  snprintf(path, sizeof(path), "/sys/block/%s/device/unload_heads", name);
  fd = open(path, O_RDWR);
  if (fd < 0)
    return DRIVE_GUARD_DRIVE_STATE_NONE;
  read_len = read(fd, tmp, sizeof(tmp));
  close(fd);
  if (read_len < 0)
    return DRIVE_GUARD_DRIVE_STATE_UNSUPPORTED;
  return DRIVE_GUARD_DRIVE_STATE_ENABLED;
}

/*
 */

typedef struct
{
  GSource  source;
  GPollFD *poll_fd;
}
PollSource;

static gboolean
fd_poll_source_prepare (GSource *source, gint *timeout)
{
  *timeout = -1;
  return FALSE;
}

static gboolean
fd_poll_source_check (GSource *source)
{
  PollSource *poll_source = (PollSource *) source;

  if (poll_source->poll_fd->revents & POLLIN)
    return TRUE;

  if (poll_source->poll_fd->revents & (POLLERR | POLLHUP))
    exit(1);

  return FALSE;
}

static gboolean
fd_poll_source_dispatch (GSource *source, GSourceFunc callback, gpointer user_data)
{
  callback (user_data);
  return TRUE;
}

static GSourceFuncs fd_poll_source_funcs =
{
  fd_poll_source_prepare,
  fd_poll_source_check,
  fd_poll_source_dispatch
};

static GSource *
pipe_source_new (int fd)
{
  PollSource *poll_source;

  poll_source = (PollSource *) g_source_new (&fd_poll_source_funcs, sizeof (PollSource));
  poll_source->poll_fd = g_new (GPollFD, 1);
  poll_source->poll_fd->events = G_IO_IN | G_IO_HUP | G_IO_ERR;
  poll_source->poll_fd->fd = fd;
  g_source_add_poll ((GSource *) poll_source, poll_source->poll_fd);

  return (GSource *) poll_source;
}

/* --- DriveGuard --- */

struct _DriveGuardServer {
  DBusConnection *dbus_connection;

  GMainContext *context;
  GMainLoop *main_loop;
  GSource *pipe_source;
  guint pipe_source_id;

  DriveState default_state;
  DriveState current_state;

  DriveGuardServerConfig config;

  pid_t core_pid;
  int shmfd;
  struct hp_drive_guard_shm *shm;

  LaptopHW *laptop_hw;
  int current_timeout;

  PolkitAuthority *auth;

  char *core_argv[MAX_DRIVE_GUARD_DEVICES + 3];
  int num_unload_devs;
};

static DriveGuardServer *
drive_guard_server_new (void)
{
  return g_new0 (DriveGuardServer, 1);
}

static void
drive_guard_server_free (DriveGuardServer *drive_guard_server)
{
  g_free (drive_guard_server);
}

static void
send_dbus_state_change_signal (DriveGuardServer *drive_guard_server)
{
  DBusMessage *message;
  guint32 n;

  message = dbus_message_new_signal (DRIVE_GUARD_DBUS_SERVER_PATH, DRIVE_GUARD_DBUS_SERVER_IFACE, "StateChanged");
  n = drive_guard_server->current_state;

  dbus_message_append_args (message, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID);
  dbus_connection_send (drive_guard_server->dbus_connection, message, NULL);
  dbus_message_unref (message);
}

static gboolean
handle_shock_event (DriveGuardServer *drive_guard_server)
{
  DriveState state;
  unsigned int val;

  state = drive_guard_server->current_state;
  if (!daemon_comm_read (&drive_guard_server->shm->state, 1, &val))
    return TRUE;

  drive_guard_server->current_state = val & 0x0f;
  if (state != drive_guard_server->current_state)
    send_dbus_state_change_signal (drive_guard_server);

  return TRUE;
}

static gboolean drive_guard_server_init_dbus (DriveGuardServer *drive_guard);

static gboolean
retry_dbus_init (DriveGuardServer *drive_guard)
{
  drive_guard_server_init_dbus (drive_guard);
  return FALSE;
}

static gboolean
check_polkit_authorized (DBusMessage *message,
			 DriveGuardServer *drive_guard)
{
  const char *sender;
  PolkitSubject *subject;
  PolkitAuthorizationResult *result;
  gboolean authed;
  GError *error = NULL;

  sender = dbus_message_get_sender (message);
  subject = polkit_system_bus_name_new (sender);
  result = polkit_authority_check_authorization_sync (drive_guard->auth,
                                                      subject,
                                                      DRIVE_GUARD_POLICY_ACTION_TOGGLE,
                                                      NULL,
                                                      POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION,
                                                      NULL, &error);
  g_object_unref (subject);
  if (error) {
    g_error_free (error);
    return FALSE;
  }

  authed = polkit_authorization_result_get_is_authorized (result);
  g_object_unref (result);
  return authed;
}

static void drive_guard_start_core (DriveGuardServer *drive_guard);
static void drive_guard_stop_core (DriveGuardServer *drive_guard);

static void
init_core_argv (DriveGuardServer *drive_guard)
{
  memset (drive_guard->core_argv, 0, sizeof(drive_guard->core_argv));
  drive_guard->core_argv[0] = DRIVE_GUARD_CORE_PROG_NAME;
  drive_guard->core_argv[1] = "0";
  drive_guard->num_unload_devs = 0;
}

static void
add_unload_device (DriveGuardServer *drive_guard, const gchar *block)
{
  DriveState state;

  state = check_device_state (block);
  if (state == DRIVE_GUARD_DRIVE_STATE_ENABLED) {
    if (drive_guard->num_unload_devs >= MAX_DRIVE_GUARD_DEVICES) {
      fprintf (stderr, "%s: too many devices\n", progname);
      exit (1);
    }
    drive_guard->core_argv[drive_guard->num_unload_devs + 2] = block;
    drive_guard->num_unload_devs++;
  }

  if ((int)state > (int)drive_guard->default_state)
    drive_guard->default_state = state;
}

static void
setup_block_devices (DriveGuardServer *drive_guard_server,
                     DriveGuardServerConfig *config)
{
  static char tmp[32];

  init_core_argv (drive_guard_server);
  snprintf(tmp, sizeof(tmp), "%d", config->sched_prio);
  drive_guard_server->core_argv[1] = tmp;
  drive_guard_server->default_state = DRIVE_GUARD_DRIVE_STATE_NONE;
  if (!config->num_devices)
    add_unload_device(drive_guard_server, "sda");
  else {
    int i;
    for (i = 0; i < config->num_devices; i++)
      add_unload_device(drive_guard_server, config->devices[i]);
  }
}

static DBusHandlerResult
handle_dbus_server_message (DBusConnection *conn, DBusMessage *message, DriveGuardServer *drive_guard_server)
{
  return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static DBusHandlerResult
handle_dbus_message (DBusConnection *conn, DBusMessage *message, DriveGuardServer *drive_guard_server)
{
  DBusError error;
  DBusHandlerResult result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

  dbus_error_init (&error);

  if (dbus_message_is_method_call (message, DRIVE_GUARD_DBUS_SERVER_NAME, "GetState"))
  {
    DBusMessage *reply;
    guint32 n;

    if (!dbus_message_get_args (message, &error, DBUS_TYPE_INVALID))
    {
      g_warning ("Could not parse Server.GetState message.");
      goto fail;
    }

    n = drive_guard_server->current_state;

    reply = dbus_message_new_method_return (message);
    dbus_message_append_args (reply, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID);
    dbus_connection_send (conn, reply, NULL);
    dbus_message_unref (reply);

    result = DBUS_HANDLER_RESULT_HANDLED;
  }
  else if (dbus_message_is_signal (message, DBUS_INTERFACE_LOCAL, "Disconnected"))
  {
    laptop_hw_status_free(drive_guard_server->laptop_hw);
    drive_guard_server->laptop_hw = NULL;
    dbus_connection_close (drive_guard_server->dbus_connection);
    dbus_connection_unref (drive_guard_server->dbus_connection);
    drive_guard_server->dbus_connection = NULL;
    retry_dbus_init (drive_guard_server);
    result = DBUS_HANDLER_RESULT_HANDLED;
  }
  else if (dbus_message_is_method_call (message, DRIVE_GUARD_DBUS_SERVER_NAME, "Enable"))
  {
    guint32 n;

    if (!check_polkit_authorized (message, drive_guard_server)) {
      goto fail;
    }
    
    if (!dbus_message_get_args (message, &error, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID))
    {
      g_warning ("Could not parse Server.Enable args.");
      goto fail;
    }

    if (n) {
      if (drive_guard_server->current_state == DRIVE_GUARD_DRIVE_STATE_STOPPED) {
        drive_guard_server->current_state = drive_guard_server->default_state;
        drive_guard_start_core (drive_guard_server);
        send_dbus_state_change_signal (drive_guard_server);
      }
    } else {
      if (drive_guard_server->current_state >= DRIVE_GUARD_DRIVE_STATE_ENABLED) {
        drive_guard_stop_core (drive_guard_server);
        drive_guard_server->current_state = DRIVE_GUARD_DRIVE_STATE_STOPPED;
        send_dbus_state_change_signal (drive_guard_server);
      }
    }

    result = DBUS_HANDLER_RESULT_HANDLED;
  }
  else if (dbus_message_is_method_call (message, DRIVE_GUARD_DBUS_SERVER_NAME, "Reset"))
  {
    guint32 n;

    if (!dbus_message_get_args (message, &error, DBUS_TYPE_INVALID))
    {
      g_warning ("Could not parse Server.Reset message.");
      goto fail;
    }

    drive_guard_stop_core (drive_guard_server);
    drive_guard_free_config (&drive_guard_server->config);
    drive_guard_read_config (&drive_guard_server->config);
    setup_block_devices (drive_guard_server, &drive_guard_server->config);
    if (drive_guard_server->config.enabled)
      drive_guard_server->current_state = drive_guard_server->default_state;
    else
      drive_guard_server->current_state = DRIVE_GUARD_DRIVE_STATE_DISABLED;
    if (drive_guard_server->current_state >= DRIVE_GUARD_DRIVE_STATE_ENABLED)
      drive_guard_start_core (drive_guard_server);
    send_dbus_state_change_signal (drive_guard_server);

    result = DBUS_HANDLER_RESULT_HANDLED;
  }

fail:
  if (dbus_error_is_set (&error))
    dbus_error_free (&error);

  return result;
}

static gboolean
drive_guard_server_init_dbus (DriveGuardServer *drive_guard_server)
{
  DBusConnection *conn;
  DBusError error;
  const DBusObjectPathVTable dbus_server_vtable =
  {
    NULL,
    (DBusObjectPathMessageFunction) handle_dbus_server_message,
    NULL,
    NULL,
    NULL,
    NULL
  };

  if (drive_guard_server->dbus_connection)
    return TRUE;

  dbus_error_init (&error);

  drive_guard_server->dbus_connection = conn = dbus_bus_get_private (DBUS_BUS_SYSTEM, &error);
  if (!conn)
  {
    g_warning ("Could not connect to D-Bus system bus.");
    goto fail;
  }

  dbus_connection_set_exit_on_disconnect (conn, FALSE);
  dbus_connection_setup_with_g_main (conn,
                                     g_main_loop_get_context (drive_guard_server->main_loop));

  if (dbus_bus_request_name (conn,
                             DRIVE_GUARD_DBUS_NAME,
                             DBUS_NAME_FLAG_DO_NOT_QUEUE,
                             &error) != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
  {
    g_warning ("Did not get a D-Bus name.");
    goto fail;
  }

  if (!dbus_connection_add_filter (conn,
                                   (DBusHandleMessageFunction) handle_dbus_message,
                                   drive_guard_server,
                                   NULL))
  {
    g_warning ("Could not add D-Bus message filter.");
    goto fail;
  }

  if (!dbus_connection_register_object_path (conn,
                                             DRIVE_GUARD_DBUS_SERVER_PATH,
                                             &dbus_server_vtable,
                                             drive_guard_server))
  {
    g_warning ("Could not register D-Bus object path.");
    goto fail;
  }

  send_dbus_state_change_signal (drive_guard_server);

  return TRUE;

fail:

  if (dbus_error_is_set (&error))
    dbus_error_free (&error);

  if (conn)
  {
    dbus_connection_close (conn);
    dbus_connection_unref (conn);
    drive_guard_server->dbus_connection = NULL;
  }

  g_timeout_add_seconds (DBUS_RETRY_INTERVAL_S, (GSourceFunc) retry_dbus_init, drive_guard_server);

  return FALSE;
}

/*
 * communication with the helper
 */

static void
drive_guard_update_timeout (LaptopHW *hw, LaptopHWStatus *status, void *data)
{
  DriveGuardServer *drive_guard = data;
  int timeout;

  if (status->is_on_ac || status->is_lid_open)
    timeout = drive_guard->config.default_timeout;
  else
    timeout = drive_guard->config.extended_timeout;

  if (timeout != drive_guard->current_timeout) {
    if (drive_guard->core_pid)
      daemon_comm_write (&drive_guard->shm->cmd, timeout);
    drive_guard->current_timeout = timeout;
  }
}

/* start the core helper binary */
static void
drive_guard_start_core (DriveGuardServer *drive_guard)
{
  struct hp_drive_guard_shm *shm = drive_guard->shm;

  drive_guard->core_pid = fork();
  if (drive_guard->core_pid == -1) {
    fprintf(stderr, "%s: cannot fork\n", progname);
    exit(1);
  }

  if (!drive_guard->core_pid) {
    close(shm->cmd.pipe[P_WRITER]);
    close(shm->state.pipe[P_READER]);
    munmap(shm, sizeof(*shm));
    close(drive_guard->shmfd);
    execv(CORE_EXEC_PATH, drive_guard->core_argv);
    fprintf(stderr, "%s: cannot exec %s\n", progname, CORE_EXEC_PATH);
    exit(1);
  }
  
  if (drive_guard->laptop_hw) {
    drive_guard->current_timeout = 0; /* force to reset */
    laptop_hw_status_update(drive_guard->laptop_hw);
  }
}

/* stop the core helper binary */
static void
drive_guard_stop_core (DriveGuardServer *drive_guard)
{
  if (drive_guard->core_pid) {
    int status;
    daemon_comm_write (&drive_guard->shm->cmd, 0);
    waitpid (drive_guard->core_pid, &status, 0);
    drive_guard->core_pid = 0;
  }
}

static void
drive_guard_server_init (DriveGuardServer *drive_guard_server)
{
  DriveGuardServerConfig *config = &drive_guard_server->config;
  int freefall_fd;
  int shmfd;
  struct hp_drive_guard_shm *shm;
  GError *error = NULL;

  freefall_fd = open (FREEFALL_WATCH_DEVICE, O_RDONLY | O_NONBLOCK);
  if (freefall_fd < 0) {
    fprintf(stderr, "%s: cannot open device file %s\n", progname,
            FREEFALL_WATCH_DEVICE);
    exit(1);
  }

  drive_guard_init_config (config);
  drive_guard_read_config (config);
  setup_block_devices (drive_guard_server, config);
  drive_guard_server->current_state = drive_guard_server->default_state;

  shmfd = shm_open (DRIVE_GUARD_SHM_PATH, O_RDWR | O_CREAT | O_TRUNC, 0600);
  if (shmfd < 0) {
    fprintf(stderr, "%s: cannot create SHM\n", progname);
    exit(1);
  }
  if (ftruncate(shmfd, sizeof(*shm)) < 0) {
    fprintf(stderr, "%s: cannot initialize SHM\n", progname);
    exit(1);
  }

  shm = mmap(NULL, sizeof(*shm), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
  if (shm == MAP_FAILED || !shm) {
    fprintf(stderr, "%s: cannot mmap SHM for fd %d\n", progname, shmfd);
    exit(1);
  }
  memset(shm, 0, sizeof(*shm));
  drive_guard_server->shmfd = shmfd;
  drive_guard_server->shm = shm;
  shm->freefall_fd = freefall_fd;

  if (pipe(shm->state.pipe) < 0 || pipe(shm->cmd.pipe) < 0) {
    fprintf(stderr, "%s: cannot create a pipe\n", progname);
    exit(1);
  }

  drive_guard_server->context = g_main_context_default ();
  drive_guard_server->main_loop = g_main_loop_new (drive_guard_server->context, FALSE);

  drive_guard_server->auth = polkit_authority_get_sync (NULL, &error);

  drive_guard_server_init_dbus (drive_guard_server);

  drive_guard_server->laptop_hw = laptop_hw_status_new(drive_guard_update_timeout, drive_guard_server);
  if (drive_guard_server->laptop_hw) {
    DBusConnection *bus = laptop_hw_get_dbus_connection(drive_guard_server->laptop_hw);
    if (bus)
      dbus_connection_setup_with_g_main (bus,
        g_main_loop_get_context (drive_guard_server->main_loop));
  }

  if (!drive_guard_server->config.enabled)
    drive_guard_server->current_state = DRIVE_GUARD_DRIVE_STATE_DISABLED;

  drive_guard_server->pipe_source = pipe_source_new (shm->state.pipe[P_READER]);
  g_source_set_callback (drive_guard_server->pipe_source,
                         (GSourceFunc) handle_shock_event,
                         drive_guard_server, NULL);
  drive_guard_server->pipe_source_id = g_source_attach (drive_guard_server->pipe_source,
                                                        drive_guard_server->context);

  if (drive_guard_server->current_state >= DRIVE_GUARD_DRIVE_STATE_ENABLED)
    drive_guard_start_core (drive_guard_server);
}

static void
drive_guard_server_run (DriveGuardServer *drive_guard_server)
{
  g_main_loop_run (drive_guard_server->main_loop);
}

#if 0
static void
drive_guard_server_quit (DriveGuardServer *drive_guard_server)
{
  g_main_loop_quit (drive_guard_server->main_loop);
}
#endif

/* --- Main --- */

gint
main (gint argc, gchar *argv [])
{
  DriveGuardServer *drive_guard_server;

  g_type_init ();

  drive_guard_server = drive_guard_server_new ();

  drive_guard_server_init (drive_guard_server);
  drive_guard_server_run (drive_guard_server);

  drive_guard_server_free (drive_guard_server);
  return 0;
}
