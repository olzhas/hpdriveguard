/*
 * hp-drive-guard daemon
 * 
 * AC-cable and LID status check via upower
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>

#include <libupower-glib/upower.h>
#include <dbus/dbus.h>

#include "laptop-status.h"

struct LaptopHW {
	UpClient *client;
	struct LaptopHWStatus status;
	LaptopHWCallback callback;
	void *user_data;
};

static gboolean is_lid_open(LaptopHW *hw)
{
	gboolean lid_is_closed;
	g_object_get(hw->client, "lid-is-closed", &lid_is_closed, NULL);
	return !lid_is_closed;
}

static gboolean is_on_ac(LaptopHW *hw)
{
	gboolean on_battery;
	g_object_get(hw->client, "on-battery", &on_battery, NULL);
	return !on_battery;
}

void laptop_hw_status_update(LaptopHW *hw)
{
	if (hw->callback)
		hw->callback(hw, &hw->status, hw->user_data); 
}

static void
client_changed_cb (UpClient *client, LaptopHW *hw)
{
	int prev_lid_open = hw->status.is_lid_open;
	int prev_on_ac = hw->status.is_on_ac;

	hw->status.is_lid_open = is_lid_open(hw);
	hw->status.is_on_ac = is_on_ac(hw);
	if (prev_lid_open != hw->status.is_lid_open ||
	    prev_on_ac != hw->status.is_on_ac)
		laptop_hw_status_update(hw);
}

LaptopHW *
laptop_hw_status_new(LaptopHWCallback callback, void *user_data)
{
	LaptopHW *hw;

	hw = g_new0(LaptopHW, 1);
	hw->client = up_client_new();
	g_signal_connect(hw->client, "changed",
			 G_CALLBACK(client_changed_cb), hw);
	hw->status.is_lid_open = is_lid_open(hw);
	hw->status.is_on_ac = is_on_ac(hw);
	hw->user_data = user_data;
	hw->callback = callback;
	return hw;
}

void laptop_hw_status_free(LaptopHW *hw)
{
	if (!hw)
		return;
	g_object_unref(hw->client);
	g_free(hw);
}

DBusConnection *laptop_hw_get_dbus_connection(LaptopHW *hw)
{
	return NULL;
}
