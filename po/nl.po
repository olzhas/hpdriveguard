# English for hp-drive-guard package.
# Copyright (C) 2010 Hewlett-Packard
# This file is distributed under the same license as the hp-drive-guard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: hp-drive-guard 0.3.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-09-01 08:59+0200\n"
"PO-Revision-Date: 2010-08-26 12:48+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Dutch\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/client.c:159
msgid "Hard drives temporarily halted (parked)"
msgstr "Vaste schijven tijdelijk uitgeschakeld (geparkeerd)"

#: src/client.c:160
msgid ""
"The computer's internal hard drive has been temporarily halted (parked) to "
"protect the drive from data corruption. The drive will return to normal "
"operation when there are no more sudden movements."
msgstr ""
"De interne vaste schijf van de computer is tijdelijk uitgeschakeld "
"(geparkeerd) om te voorkomen dat er gegevens beschadigd raken. De vaste "
"schijf functioneert weer normaal wanneer zich geen plotselinge bewegingen "
"meer voordoen."

#: src/client.c:165
msgid "Hard drives re-enabled"
msgstr "Vaste schijven weer ingeschakeld"

#: src/client.c:166
msgid ""
"The computer's hard drive has been re-enabled after being temporarily halted "
"(parked) due to external impact or shaking."
msgstr ""
"De interne vaste schijf van de computer is weer ingeschakeld. De vaste "
"schijf was tijdelijk uitgeschakeld (geparkeerd) in verband met stoten tegen "
"de computer of schokkende bewegingen."

#: src/client.c:173
msgid "No hard drives are available."
msgstr "Er zijn momenteel geen vaste schijven beschikbaar."

#: src/client.c:175
msgid "The drives attached to your PC are not supported."
msgstr ""
"De schijfeenheden die op de computer zijn aangesloten, worden niet "
"ondersteund."

#: src/client.c:177
msgid ""
"Your computer includes a Solid State Drive (SSD). Hard drive protection is "
"not required on Solid State Drives."
msgstr ""
"Uw computer is uitgerust met een Solid State Drive (SSD). Voor Solid State "
"Drives is geen vasteschijfbeveiliging vereist."

#: src/client.c:180
msgid "HP DriveGuard has been disabled by the user."
msgstr "HP DriveGuard is uitgeschakeld door de gebruiker."

#: src/client.c:182
msgid "HP DriveGuard has been stopped temporarily by the user."
msgstr "HP DriveGuard is tijdelijk uitgeschakeld door de gebruiker."

#: src/client.c:184
msgid "HP DriveGuard is enabled to protect your hard drive."
msgstr "HP DriveGuard is ingeschakeld om de vaste schijf te beschermen."

#: src/client.c:186
msgid ""
"Hard drive is temporarily halted (parked) for protection from shocks and "
"vibrations."
msgstr ""
"De vaste schijf wordt tijdelijk uitgeschakeld (geparkeerd) om deze te "
"beschermen tegen schokkende bewegingen en trillingen."

#: src/client.c:420
msgid "Licensed under the GNU General Public License Version 2"
msgstr "Licentie: GNU General Public License, versie 2"

#: src/client.c:421
msgid "HP DriveGuard Applet"
msgstr "Applet HP DriveGuard"

#: src/client.c:470
msgid "_System Setup"
msgstr "_Systeemconfiguratie"

#: src/client.c:471
msgid "Setup HP DriveGuard service"
msgstr "HP DriveGuard configureren"

#: src/client.c:474
msgid "_Quit"
msgstr "_Afsluiten"

#: src/client.c:475
msgid "Quit"
msgstr "Afsluiten"

#: src/client.c:478
msgid "_About"
msgstr "_Info"

#: src/client.c:479
msgid "About"
msgstr "Info"

#: src/client.c:529
msgid "Start Protection"
msgstr "Bescherming starten"

#: src/client.c:530
msgid "Start HP DriveGuard again"
msgstr "HP DriveGuard opnieuw starten"

#: src/client.c:536
msgid "Pause Protection"
msgstr "Bescherming onderbreken"

#: src/client.c:537
msgid "Stop HP DriveGuard temporarily"
msgstr "HP DriveGuard tijdelijk uitschakelen"

#: src/client.c:592
msgid "Looking for HP DriveGuard service"
msgstr "Zoeken naar HP DriveGuard"

#: src/setup.c:165
msgid "HP DriveGuard Setup"
msgstr "HP DriveGuard configureren"

#: src/setup.c:176
msgid "Enable HP DriveGuard"
msgstr "HP DriveGuard inschakelen"

#: src/setup.c:198
msgid "Default Timeout"
msgstr "Standaardtime-out"

#: src/setup.c:200
msgid "Extended Timeout"
msgstr "Uitgebreide time-out"

#: src/setup.c:202
msgid "Daemon Task Priority"
msgstr "Prioriteit daemon-taak"

#: src/setup.c:209
msgid "Devices"
msgstr "Apparaten"
