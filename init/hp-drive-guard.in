#! /bin/sh
### BEGIN INIT INFO
# Provides:          hp-drive-guard
# Required-Start:    $network $remote_fs dbus haldaemon
# Required-Stop:     $network $remote_fs dbus haldaemon
# Default-Start:     2 3 5
# Default-Stop:
# Short-Description: HP Drive Guard daemon
# Description:       HP Drive Guard daemon, parks drive heads if computer is handled roughly
### END INIT INFO

DAEMON=@sbindir@/hp-drive-guard-daemon
PIDFILE=/var/run/hp-drive-guard-daemon.pid

test -x $DAEMON || exit 5

. /etc/rc.status
rc_reset

case "$1" in
	start)
		echo -n "Starting HP Drive Guard daemon "
		if ! /sbin/checkproc $DAEMON; then
			ulimit -c 0 2>&1 >/dev/null
			/sbin/startproc $DAEMON
		fi
		rc_status -v
		;;
	stop)
		echo -n "Shutting down HP Drive Guard daemon "
		/sbin/killproc -TERM $DAEMON
		rc_status -v
		;;
	try-restart|condrestart)
		if test "$1" = "condrestart"; then
			echo "${attn} Use try-restart ${done}(LSB)${attn} rather than condrestart ${warn}(RH)${norm}"
		fi
		$0 status
		if test $? = 0; then
			$0 restart
		else
			rc_reset
		fi
		rc_status
		;;
	restart)
		$0 stop
		$0 start
		rc_status
		;;
	force-reload|reload)
		$0 restart
		rc_status -v
		;;
	status)
		echo -n "Checking for HP Drive Guard daemon: "
		/sbin/checkproc $DAEMON
		rc_status -v
		;;
	*)
		echo "Usage: $0 {start|stop|status|try-restart|restart|force-reload|reload}"
		exit 1
		;;
esac

rc_exit
