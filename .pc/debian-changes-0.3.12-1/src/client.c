/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * hp-drive-guard client applet
 *
 *   Copyright (c) 2010 Hans Petter Jansson <hpj@novell.com>
 *                      Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libnotify/notify.h>

#ifndef NOTIFY_CHECK_VERSION
#define NOTIFY_CHECK_VERSION(x, y, z)   0
#endif

#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "../config.h"
#include "common.h"

#define DBUS_RETRY_INTERVAL_S 10

/* --- GUI --- */

#define DRIVE_ICON_NAME "harddrive"
#define ICON_SIZE_DEFAULT 24

static GdkPixbuf *
make_drive_state_pixbuf (DriveState state)
{
  static const gchar * const state_icon_name [DRIVE_GUARD_DRIVE_STATE_MAX] =
  {
    [DRIVE_GUARD_DRIVE_STATE_DISABLED] = "stop",
    [DRIVE_GUARD_DRIVE_STATE_SSD_ONLY] = "stop",
    [DRIVE_GUARD_DRIVE_STATE_STOPPED] = "stop",
    [DRIVE_GUARD_DRIVE_STATE_ENABLED] = "up",  /* FIXME: Need a better symbol here */
    [DRIVE_GUARD_DRIVE_STATE_PARKED] = "important"   /* FIXME: Need a better symbol here */
  };
  GdkPixbuf *base_pixbuf;
  GdkPixbuf *state_pixbuf;
  GtkIconTheme *icon_theme;

  icon_theme = gtk_icon_theme_get_default ();

  base_pixbuf = gtk_icon_theme_load_icon (icon_theme,
                                          DRIVE_ICON_NAME,
                                          ICON_SIZE_DEFAULT,
                                          GTK_ICON_LOOKUP_FORCE_SIZE,  /* GtkIconLookupFlags */
                                          NULL);                       /* GError */
  if (!base_pixbuf)
    return NULL;

  base_pixbuf = gdk_pixbuf_copy (base_pixbuf);

  if (!state_icon_name [state])
    return base_pixbuf;

  state_pixbuf = gtk_icon_theme_load_icon (icon_theme,
                                           state_icon_name [state],
                                           ICON_SIZE_DEFAULT,
                                           GTK_ICON_LOOKUP_FORCE_SIZE,  /* GtkIconLookupFlags */
                                           NULL);                       /* GError */
  if (!state_pixbuf)
    return base_pixbuf;

  gdk_pixbuf_composite (state_pixbuf,
                        base_pixbuf,
                        ICON_SIZE_DEFAULT / 2, ICON_SIZE_DEFAULT / 2, ICON_SIZE_DEFAULT / 2, ICON_SIZE_DEFAULT / 2,
                        ICON_SIZE_DEFAULT / 2.0, ICON_SIZE_DEFAULT / 2.0, 0.5, 0.5,
                        GDK_INTERP_BILINEAR,
                        255);

  g_object_unref (state_pixbuf);
  return base_pixbuf;
}

/* --- DriveGuard --- */

typedef struct
{
  GtkStatusIcon *status_icon;
  GMainLoop *main_loop;
  GSource *freefall_source;
  guint freefall_source_id;
  guint shock_timeout_id;
  guint notification_idle_id;
  gboolean allow_user_setup;

  GdkPixbuf *state_pixbuf [DRIVE_GUARD_DRIVE_STATE_MAX];
  DriveState last_state;
  DriveState current_state;
  gint current_tooltip;

  gint current_notification;
  NotifyNotification *notifications [2];

  DBusConnection *dbus_connection;
}
DriveGuard;

static DriveGuard *
drive_guard_new (void)
{
  return g_new0 (DriveGuard, 1);
}

static void
drive_guard_free (DriveGuard *drive_guard)
{
  gint i;

  for (i = 0; i < DRIVE_GUARD_DRIVE_STATE_MAX; i++)
  {
    g_object_unref (drive_guard->state_pixbuf [i]);
  }

  g_free (drive_guard);
}

static struct
{
  NotifyUrgency  urgency;
  gint           timeout;
  const gchar   *icon;
  const gchar   *summary;
  const gchar   *body;
} message [2] =
  {
    { NOTIFY_URGENCY_CRITICAL, NOTIFY_EXPIRES_NEVER, "important",
      N_("Hard drives temporarily halted (parked)"),
      N_("The computer's internal hard drive has been temporarily halted "
         "(parked) to protect the drive from data corruption. The drive will "
         "return to normal operation when there are no more sudden movements.") },

    { NOTIFY_URGENCY_CRITICAL, NOTIFY_EXPIRES_DEFAULT, "up",
      N_("Hard drives re-enabled"),
      N_("The computer's hard drive has been re-enabled after being "
         "temporarily halted (parked) due to external impact or shaking.") },
  };

static const gchar *tooltip_msgs [DRIVE_GUARD_DRIVE_STATE_MAX] =
  {
    [DRIVE_GUARD_DRIVE_STATE_NONE] =
    N_("No hard drives are available."),
    [DRIVE_GUARD_DRIVE_STATE_UNSUPPORTED] =
    N_("The drives attached to your PC are not supported."),
    [DRIVE_GUARD_DRIVE_STATE_SSD_ONLY] =
    N_("Your computer includes a Solid State Drive (SSD). "
       "Hard drive protection is not required on Solid State Drives."),
    [DRIVE_GUARD_DRIVE_STATE_DISABLED] =
    N_("HP DriveGuard has been disabled by the user."),
    [DRIVE_GUARD_DRIVE_STATE_STOPPED] =
    N_("HP DriveGuard has been stopped temporarily by the user."),
    [DRIVE_GUARD_DRIVE_STATE_ENABLED] =
    N_("HP DriveGuard is enabled to protect your hard drive."),
    [DRIVE_GUARD_DRIVE_STATE_PARKED] =
    N_("Hard drive is temporarily halted (parked) for protection from "
       "shocks and vibrations."),
  };

static void
init_state_change_notification (DriveGuard *drive_guard)
{
  int n;

  drive_guard->current_notification = -1;
  for (n = 0; n < 2; n++) {
#if NOTIFY_CHECK_VERSION(0, 7, 0)
    drive_guard->notifications [n] =
    notify_notification_new (gettext (message [n].summary),
                             gettext (message [n].body),
                             message [n].icon);
#else
    drive_guard->notifications [n] =
    notify_notification_new_with_status_icon (gettext (message [n].summary),
                                              gettext (message [n].body),
                                              message [n].icon,
                                              drive_guard->status_icon);
#endif
    notify_notification_set_urgency (drive_guard->notifications [n], message [n].urgency);
    notify_notification_set_timeout (drive_guard->notifications [n], message [n].timeout);
    /* first show to let system caching... */
    notify_notification_show (drive_guard->notifications [n], NULL);
    notify_notification_close (drive_guard->notifications [n], NULL);
  }
}

static gboolean
present_state_change_notification (DriveGuard *drive_guard)
{
  gint n = -1;

  if (drive_guard->current_state == DRIVE_GUARD_DRIVE_STATE_PARKED &&
      drive_guard->last_state != DRIVE_GUARD_DRIVE_STATE_PARKED)
    n = 0;
  else if (drive_guard->current_state == DRIVE_GUARD_DRIVE_STATE_ENABLED &&
      drive_guard->last_state == DRIVE_GUARD_DRIVE_STATE_PARKED)
    n = 1;

  if (n >= 0)
  {
    if (drive_guard->current_notification != -1 &&
        n != drive_guard->current_notification)
      notify_notification_close (drive_guard->notifications [drive_guard->current_notification], NULL);
    notify_notification_show (drive_guard->notifications [n], NULL);
    drive_guard->current_notification = n;
  } else {
    if (drive_guard->current_notification != -1 &&
        drive_guard->current_state != DRIVE_GUARD_DRIVE_STATE_PARKED &&
	drive_guard->current_state != DRIVE_GUARD_DRIVE_STATE_ENABLED) {
      notify_notification_close (drive_guard->notifications [drive_guard->current_notification], NULL);
      drive_guard->current_notification = -1;
    }
  }

  if (drive_guard->current_tooltip != drive_guard->current_state)
  {
    drive_guard->current_tooltip = drive_guard->current_state;
    gtk_status_icon_set_tooltip_text (drive_guard->status_icon,
				 gettext (tooltip_msgs [drive_guard->current_tooltip]));
  }

  drive_guard->notification_idle_id = 0;
  return FALSE;
}

static void
queue_state_change_notification (DriveGuard *drive_guard)
{
  if (drive_guard->notification_idle_id)
    return;

  drive_guard->notification_idle_id =
    g_idle_add_full (G_PRIORITY_LOW,
                     (GSourceFunc) present_state_change_notification,
                     drive_guard,
                     NULL);
}

static void
drive_guard_update_state (DriveGuard *drive_guard, DriveState current_state)
{
  if (drive_guard->current_state == current_state)
    return;

  drive_guard->last_state = drive_guard->current_state;
  drive_guard->current_state = current_state;

  gtk_status_icon_set_from_pixbuf (drive_guard->status_icon, drive_guard->state_pixbuf [drive_guard->current_state]);
  queue_state_change_notification (drive_guard);
}

static gboolean drive_guard_init_dbus (DriveGuard *drive_guard);

static gboolean
retry_dbus_init (DriveGuard *drive_guard)
{
  drive_guard_init_dbus (drive_guard);
  return FALSE;
}

static DBusHandlerResult
handle_dbus_message (DBusConnection *conn, DBusMessage *message, DriveGuard *drive_guard)
{
  DBusError error;
  DBusHandlerResult result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

  dbus_error_init (&error);

  if (dbus_message_is_signal (message, DRIVE_GUARD_DBUS_SERVER_IFACE, "StateChanged"))
  {
    guint32 n;

    if (!dbus_message_get_args (message, &error, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID))
    {
      g_warning ("Could not parse Server.StateChanged signal.");
      goto fail;
    }

    drive_guard_update_state (drive_guard, n);

    result = DBUS_HANDLER_RESULT_HANDLED;
  }
  else if (dbus_message_get_type (message) == DBUS_MESSAGE_TYPE_METHOD_RETURN)
  {
    guint32 n;

    if (!dbus_message_get_args (message, &error, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID))
    {
      g_warning ("Could not parse Server.GetState reply.");
      goto fail;
    }

    drive_guard_update_state (drive_guard, n);

    result = DBUS_HANDLER_RESULT_HANDLED;
  }
  else if (dbus_message_is_signal (message, DBUS_INTERFACE_LOCAL, "Disconnected"))
  {
    dbus_connection_unref (drive_guard->dbus_connection);
    drive_guard->dbus_connection = NULL;
    retry_dbus_init (drive_guard);
    result = DBUS_HANDLER_RESULT_HANDLED;
  }

fail:

  return result;
}

static void
ask_dbus_for_state (DriveGuard *drive_guard)
{
  DBusMessage *message;

  message = dbus_message_new_method_call (DRIVE_GUARD_DBUS_NAME,
                                          DRIVE_GUARD_DBUS_SERVER_PATH,
                                          DRIVE_GUARD_DBUS_SERVER_IFACE,
                                          "GetState");
  dbus_connection_send (drive_guard->dbus_connection, message, NULL);
  dbus_message_unref (message);
}

static gboolean
drive_guard_init_dbus (DriveGuard *drive_guard)
{
  DBusConnection *conn;
  DBusError error;

  if (drive_guard->dbus_connection)
    return TRUE;

  dbus_error_init (&error);

  drive_guard->dbus_connection = conn = dbus_bus_get (DBUS_BUS_SYSTEM, &error);
  if (!conn)
  {
    g_warning ("Could not connect to D-Bus system bus.");
    goto fail;
  }

  dbus_connection_set_exit_on_disconnect (conn, FALSE);
  dbus_connection_setup_with_g_main (conn,
                                     g_main_loop_get_context (drive_guard->main_loop));

  if (!dbus_connection_add_filter (conn,
                                   (DBusHandleMessageFunction) handle_dbus_message,
                                   drive_guard,
                                   NULL))
  {
    g_warning ("Could not add D-Bus message filter.");
    goto fail;
  }

  dbus_bus_add_match (conn, "path='" DRIVE_GUARD_DBUS_SERVER_PATH "'", &error);
  if (dbus_error_is_set (&error))
  {
    g_warning ("Could not add D-Bus message match.");
    goto fail;
  }

  ask_dbus_for_state (drive_guard);

  return TRUE;

fail:

  if (dbus_error_is_set (&error))
    dbus_error_free (&error);

  if (conn)
  {
    dbus_connection_unref (conn);
    drive_guard->dbus_connection = NULL;
  }

  g_timeout_add_seconds (DBUS_RETRY_INTERVAL_S, (GSourceFunc) retry_dbus_init, drive_guard);

  return FALSE;
}

/*
 * popup menu
 */
static void
drive_guard_about_cb (GtkAction *item, gpointer data)
{
  static const char *authors[] = {
    "Hans Petter Jansson <hpj@novell.com>",
    "Takashi Iwai <tiwai@suse.de>",
    NULL
  };

  gtk_window_set_default_icon_name (DRIVE_ICON_NAME);
  gtk_show_about_dialog (NULL,
			 "version", VERSION,
			 "copyright", "Copyright \xc2\xa9 2010 Hans Petter Jansson, Takashi Iwai",
			 "license", _("Licensed under the GNU General Public License Version 2"),
			 "comments", _("HP DriveGuard Applet"),
			 "authors", authors,
			 "logo-icon-name", DRIVE_ICON_NAME,
			 NULL);
}

static void
send_enable_method (DriveGuard *drive_guard, gboolean enable)
{
  DBusMessage *message;
  guint n = enable;

  message = dbus_message_new_method_call (DRIVE_GUARD_DBUS_NAME,
                                          DRIVE_GUARD_DBUS_SERVER_PATH,
                                          DRIVE_GUARD_DBUS_SERVER_IFACE,
                                          "Enable");
  dbus_message_append_args (message, DBUS_TYPE_UINT32, &n, DBUS_TYPE_INVALID);
  dbus_connection_send (drive_guard->dbus_connection, message, NULL);
  dbus_message_unref (message);
}

static void
drive_guard_start_protection_cb (GtkMenuItem *item, gpointer data)
{
  send_enable_method ((DriveGuard *)data, TRUE);
}

static void
drive_guard_stop_protection_cb (GtkMenuItem *item, gpointer data)
{
  send_enable_method ((DriveGuard *)data, FALSE);
}



static void
drive_guard_quit_cb (GtkMenuItem *item, gpointer data)
{
  exit (0);
}

static void
drive_guard_setup_cb (GtkMenuItem *item, gpointer data)
{
  drive_guard_do_setup (((DriveGuard *)data)->allow_user_setup);
}

static GtkActionEntry entries[] = {
  { "setup", GTK_STOCK_PREFERENCES,
    N_("_System Setup"), "<control>S",
    N_("Setup HP DriveGuard service"), /* tooltip */
    G_CALLBACK (drive_guard_setup_cb) },
  { "start", GTK_STOCK_GO_FORWARD,
    N_("Start Protection"), "<control>F",
    N_("Start HP DriveGuard again"), /* tooltip */
    G_CALLBACK (drive_guard_start_protection_cb) },
  { "stop", GTK_STOCK_STOP,
    N_("Pause Protection"), "<control>T",
    N_("Stop HP DriveGuard temporarily"), /* tooltip */
    G_CALLBACK (drive_guard_stop_protection_cb) },
  { "quit", GTK_STOCK_QUIT,
    N_("_Quit"), "<control>Q",
    N_("Quit"), /* tooltip */
    G_CALLBACK (drive_guard_quit_cb) },
  { "about", GTK_STOCK_ABOUT,
    N_("_About"), "<control>A",
    N_("About"), /* tooltip */
    G_CALLBACK (drive_guard_about_cb) },
};

static const gchar *ui_start_info = 
"<ui>"
"  <popup name='Popup'>"
"    <menuitem action='start'/>"
"    <menuitem action='setup'/>"
"    <menuitem action='about'/>"
"    <menuitem action='quit'/>"
"  </popup>"
"</ui>";

static const gchar *ui_stop_info = 
"<ui>"
"  <popup name='Popup'>"
"    <menuitem action='stop'/>"
"    <menuitem action='setup'/>"
"    <menuitem action='about'/>"
"    <menuitem action='quit'/>"
"  </popup>"
"</ui>";

static const gchar *ui_info = 
"<ui>"
"  <popup name='Popup'>"
"    <menuitem action='setup'/>"
"    <menuitem action='about'/>"
"    <menuitem action='quit'/>"
"  </popup>"
"</ui>";

static void
drive_guard_popup_menu_cb (GtkStatusIcon *status_icon, guint button,
			   guint32 timestamp, DriveGuard *drive_guard)
{
  GtkActionGroup *actions;
  GtkUIManager *ui;
  GtkWidget *menu;

  actions = gtk_action_group_new ("Actions");
  gtk_action_group_set_translation_domain (actions, PACKAGE);
  gtk_action_group_add_actions (actions, entries, G_N_ELEMENTS (entries), drive_guard);

  ui = gtk_ui_manager_new ();
  gtk_ui_manager_insert_action_group (ui, actions, 0);
  switch (drive_guard->current_state) {
  case DRIVE_GUARD_DRIVE_STATE_STOPPED:
    gtk_ui_manager_add_ui_from_string (ui, ui_start_info, -1, NULL);
    break;
  case DRIVE_GUARD_DRIVE_STATE_ENABLED:
  case DRIVE_GUARD_DRIVE_STATE_PARKED:
    gtk_ui_manager_add_ui_from_string (ui, ui_stop_info, -1, NULL);
    break;
  default:
    gtk_ui_manager_add_ui_from_string (ui, ui_info, -1, NULL);
    break;
  }

  /* show the menu */
  menu = gtk_ui_manager_get_widget (ui, "/Popup");
  gtk_widget_show_all (menu);

  gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
		  gtk_status_icon_position_menu,
		  drive_guard->status_icon,
		  button, timestamp);
}

static void
drive_guard_init (DriveGuard *drive_guard, gboolean allow_user_setup)
{
  GMainContext *context;
  gint i;

  drive_guard->last_state    = DRIVE_GUARD_DRIVE_STATE_NONE;
  drive_guard->current_state = DRIVE_GUARD_DRIVE_STATE_NONE;
  drive_guard->current_tooltip = -1;
  drive_guard->allow_user_setup = allow_user_setup;

  context = g_main_context_default ();
  drive_guard->main_loop = g_main_loop_new (context, FALSE);

  for (i = 0; i < DRIVE_GUARD_DRIVE_STATE_MAX; i++)
  {
    drive_guard->state_pixbuf [i] = make_drive_state_pixbuf (i);
  }

  drive_guard->status_icon = gtk_status_icon_new_from_pixbuf (drive_guard->state_pixbuf [DRIVE_GUARD_DRIVE_STATE_NONE]);
  gtk_status_icon_set_tooltip_text (drive_guard->status_icon,
                               _("Looking for HP DriveGuard service"));
  init_state_change_notification (drive_guard);
  queue_state_change_notification (drive_guard);

  g_signal_connect (G_OBJECT (drive_guard->status_icon),
		    "popup-menu",
		    G_CALLBACK (drive_guard_popup_menu_cb),
		    drive_guard);

  drive_guard_init_dbus (drive_guard);
}

static void
drive_guard_run (DriveGuard *drive_guard)
{
  g_main_loop_run (drive_guard->main_loop);
}

static void
drive_guard_quit (DriveGuard *drive_guard)
{
  g_main_loop_quit (drive_guard->main_loop);
}

static gboolean
freefall_exist (void)
{
  return access ("/dev/freefall", F_OK) == 0;
}

/* --- Main --- */

#ifdef DRIVE_GUARD_ALLOW_USER_SETUP
#define  setup_parameters       TRUE
#else
static gboolean setup_parameters;

static GOptionEntry options [] = {
  { "setup-parameters", 'p', 0, G_OPTION_ARG_NONE, &setup_parameters,
    "Allow user to change parameters in setup dialog" },
  { NULL }
};
#endif

gint
main (gint argc, gchar *argv [])
{
  DriveGuard *drive_guard;

  if (!freefall_exist())
    return 1;

  textdomain (PACKAGE);
  setlocale (LC_ALL, "");

  notify_init ("DriveGuard");

#ifdef DRIVE_GUARD_ALLOW_USER_SETUP
  gtk_init (&argc, &argv);
#else
  if (!gtk_init_with_args (&argc, &argv, "", options, NULL, NULL))
    return 1;
#endif

  drive_guard = drive_guard_new ();

  drive_guard_init (drive_guard, setup_parameters);
  drive_guard_run (drive_guard);

  drive_guard_free (drive_guard);
  return 0;
}
