AC_INIT(configure.ac)

dnl ----------------------------
dnl Automake/autoconf input file
dnl ----------------------------

GTK_REQUIRED=2.14.0

dnl --- Package configuration ---

AM_INIT_AUTOMAKE(hp-drive-guard, 0.3.12)
AM_CONFIG_HEADER(config.h)

AC_SUBST(VERSION)
AC_SUBST(PACKAGE)

dnl AC_SUBST(GTK_REQUIRED)
dnl AC_SUBST(GNOMEUI_REQUIRED)

AC_DEFINE_UNQUOTED(VERSION, "$VERSION", [Package version])
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE", [Package name])

AM_SANITY_CHECK
AM_MAINTAINER_MODE
AC_CANONICAL_HOST

AC_C_CONST
AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CPP
AC_PROG_INSTALL
AM_DISABLE_STATIC
AM_PROG_CC_C_O
AM_PROG_LIBTOOL

AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.17])

PKG_CHECK_MODULES(GLIB, glib-2.0)
PKG_CHECK_MODULES(GTK, gtk+-2.0 >= $GTK_REQUIRED)
PKG_CHECK_MODULES(DBUS, dbus-glib-1)
PKG_CHECK_MODULES(LIBNOTIFY, libnotify)
PKG_CHECK_MODULES(POLKITDBUS, polkit-dbus)
PKG_CHECK_MODULES(POLKITGNOME, polkit-gnome)
AC_ARG_WITH(pm,
  AS_HELP_STRING([--with-pm=hal|upower],
		 [Power-management backend, either hal or upower]),
  pm="$withval", pm="hal")
case "$pm" in
hal)
  PKG_CHECK_MODULES(HAL, hal)
  ;;
upower)
  PKG_CHECK_MODULES(UPOWER_GLIB, upower-glib)
  ;;
*)
  echo "Invalid pm backend $pm_method"
  exit 1
  ;;
esac
AM_CONDITIONAL(PM_BACKEND_HAL, test "$pm" = "hal")
AM_CONDITIONAL(PM_BACKEND_UPOWER, test "$pm" = "upower")

AC_ARG_ENABLE(user-setup, AS_HELP_STRING([--disable-user-setup], [Disable parameters in setup GUI]), user_setup="$enableval", user_setup="yes")
if test x"$user_setup" = x"yes"; then
    AC_DEFINE(DRIVE_GUARD_ALLOW_USER_SETUP,,[Allow to change parameters in setup dialog])
fi

dnl --- Set up directories ---

AC_ARG_WITH(dbus-sys, AS_HELP_STRING([--with-dbus-sys=<dir>], [Path to D-Bus system.d directory]))

if ! test -z "$with_dbus_sys"; then
  DBUS_SYS_DIR="$with_dbus_sys"
else
  DBUS_SYS_DIR="${sysconfdir}/dbus-1/system.d"
fi

AC_SUBST(DBUS_SYS_DIR)

HELPER_EXEC_DIR="${libexecdir}/hp-drive-guard"
AC_SUBST(HELPER_EXEC_DIR)

privdatadir=$(eval echo "${datadir}/hp-drive-guard")
autostartdir=$(eval echo "${sysconfdir}/xdg/autostart")
dnl GLADE requires pixmaps to live in the same dir as the GUI XML.
gladedir=$(eval echo "$privdatadir/gui")
pixmapsdir=$(eval echo "$privdatadir/gui")

AC_DEFINE_UNQUOTED(GLADE_DIR,  "$gladedir", [Glade GUI directory])
AC_DEFINE_UNQUOTED(PIXMAPS_DIR, "$pixmapsdir", [Image directory])

AC_SUBST(privdatadir)
AC_SUBST(autostartdir)
AC_SUBST(gladedir)
AC_SUBST(pixmapsdir)

dnl --- Output ---

AC_OUTPUT(Makefile                 po/Makefile.in\
          src/Makefile            \
          src/com.hp.driveguard.policy \
          init/Makefile           \
          pixmaps/Makefile)
