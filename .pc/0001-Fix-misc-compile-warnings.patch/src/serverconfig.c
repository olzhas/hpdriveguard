/*
 * hp-drive-guard daemon
 *
 *   Copyright (c) 2010 Takashi Iwai <tiwai@suse.de>
 *                      
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "common.h"
#include "serverconfig.h"

#define DRIVE_GUARD_CONFIG_FILE_PATH	"/etc/hp-drive-guard-daemon.conf"

#define DRIVE_GUARD_CONFIG_GROUP		"server"

#define DRIVE_GUARD_CONFIG_ENABLE_KEY		"enable"
#define DRIVE_GUARD_CONFIG_DEFAULT_TIMEOUT_KEY	"default_timeout"
#define DRIVE_GUARD_CONFIG_EXTENDED_TIMEOUT_KEY	"extended_timeout"
#define DRIVE_GUARD_CONFIG_SCHED_PRIO_KEY	"sched_prio"
#define DRIVE_GUARD_CONFIG_DEVICES_KEY		"devices"

int drive_guard_init_config(DriveGuardServerConfig *config)
{
	memset(config, 0, sizeof(*config));
	config->default_timeout = SHOCK_TIMEOUT_S_DEFAULT;
	config->extended_timeout = EXTENDED_TIMEOUT_S_DEFAULT;
	config->sched_prio = 50;
	config->enabled = 1;
	return TRUE;
}

void drive_guard_free_config(DriveGuardServerConfig *config)
{
	if (config->devices)
		g_strfreev((gchar **)config->devices);
	config->devices = NULL;
	config->num_devices = 0;
}

static gboolean read_config_boolean(GKeyFile *keyfile, const char *key,
				    int *valp)
{
	if (!g_key_file_has_key(keyfile, DRIVE_GUARD_CONFIG_GROUP, key, NULL))
		return FALSE;
	*valp = g_key_file_get_boolean(keyfile, DRIVE_GUARD_CONFIG_GROUP,
				       key, NULL);
	return TRUE;
}

static gboolean read_config_int(GKeyFile *keyfile, const char *key,
				int *valp, int minval, int maxval)
{
	int val;
	if (!g_key_file_has_key(keyfile, DRIVE_GUARD_CONFIG_GROUP, key, NULL))
		return FALSE;
	val = g_key_file_get_integer(keyfile, DRIVE_GUARD_CONFIG_GROUP,
				     key, NULL);
	if (val < minval || val > maxval) {
		fprintf(stderr, "config: invalid value %s for key %s\n",
			val, key);
		return FALSE;
	}
	*valp = val;
	return TRUE;
}

static void _read_config(GKeyFile *keyfile, DriveGuardServerConfig *config)
{
	gchar **devs;
	gsize nums;

	read_config_boolean(keyfile, DRIVE_GUARD_CONFIG_ENABLE_KEY,
			    &config->enabled);
	read_config_int(keyfile, DRIVE_GUARD_CONFIG_DEFAULT_TIMEOUT_KEY,
			&config->default_timeout, 1, SHOCK_TIMEOUT_S_MAX);
	read_config_int(keyfile, DRIVE_GUARD_CONFIG_EXTENDED_TIMEOUT_KEY,
			&config->extended_timeout, 1, SHOCK_TIMEOUT_S_MAX);
	read_config_int(keyfile, DRIVE_GUARD_CONFIG_SCHED_PRIO_KEY,
			&config->sched_prio, 0, 50);
	devs = g_key_file_get_string_list(keyfile, DRIVE_GUARD_CONFIG_GROUP,
					  DRIVE_GUARD_CONFIG_DEVICES_KEY,
					  &nums, NULL);
	if (devs) {
		config->num_devices = nums;
		config->devices = (const char **)devs;
	}
}

int drive_guard_read_config(DriveGuardServerConfig *config)
{
	GKeyFile *keyfile;
	gchar **devs;

	keyfile = g_key_file_new();
	if (!g_key_file_load_from_file(keyfile, DRIVE_GUARD_CONFIG_FILE_PATH,
				       G_KEY_FILE_NONE, NULL))
		return FALSE;
	_read_config(keyfile, config);
	g_key_file_free(keyfile);
	return TRUE;
}

static gboolean is_same_list(const char **list1, int num_list1,
			     const char **list2, int num_list2)
{
	if (num_list1 != num_list2)
		return FALSE;
	if (list1 != list2) {
		int n;
		if (!list1 || !list2)
			return FALSE;
		for (n = 0; n < num_list1 && *list1 && *list2; n++)
			if (strcmp(*list1, *list2))
				break;
		if (n < num_list1)
			return FALSE;
	}
	return TRUE;
}

static void write_config_int(GKeyFile *keyfile, const gchar *key,
			     int val, int minval, int maxval)
{
	if (val >= minval && val <= maxval)
		g_key_file_set_integer(keyfile, DRIVE_GUARD_CONFIG_GROUP,
				       key, val);
}

static gboolean save_config(GKeyFile *keyfile)
{
	gchar *data;
	gchar *o_data;
	gsize len;
	int fd;

	data = g_key_file_to_data(keyfile, &len, NULL);
	if (!data || !len)
		return FALSE;
	fd = open(DRIVE_GUARD_CONFIG_FILE_PATH,
		  O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fd < 0) {
		g_free(data);
		return FALSE;
	}
	o_data = data;
	while (len > 0) {
		int size = write(fd, data, len);
		if (size <= 0)
			break;
		len -= size;
		data += size;
	}
	close(fd);
	g_free(o_data);
	return !len;
}

int drive_guard_write_config(DriveGuardServerConfig *config)
{
	DriveGuardServerConfig oldconfig;
	GKeyFile *keyfile;
	gchar **devs;
	int result;

	drive_guard_init_config(&oldconfig);
	keyfile = g_key_file_new();
	if (g_key_file_load_from_file(keyfile, DRIVE_GUARD_CONFIG_FILE_PATH,
				      G_KEY_FILE_KEEP_COMMENTS |
				      G_KEY_FILE_KEEP_TRANSLATIONS,
				      NULL))
		_read_config(keyfile, &oldconfig);
	if (oldconfig.enabled != config->enabled)
		g_key_file_set_boolean(keyfile, DRIVE_GUARD_CONFIG_GROUP,
				       DRIVE_GUARD_CONFIG_ENABLE_KEY,
				       config->enabled);
	if (oldconfig.default_timeout != config->default_timeout)
		write_config_int(keyfile,
				 DRIVE_GUARD_CONFIG_DEFAULT_TIMEOUT_KEY,
				 config->default_timeout,
				 1, SHOCK_TIMEOUT_S_MAX);
	if (oldconfig.extended_timeout != config->extended_timeout)
		write_config_int(keyfile,
				 DRIVE_GUARD_CONFIG_EXTENDED_TIMEOUT_KEY,
				 config->extended_timeout,
				 1, SHOCK_TIMEOUT_S_MAX);
	if (oldconfig.sched_prio != config->sched_prio)
		write_config_int(keyfile,
				 DRIVE_GUARD_CONFIG_SCHED_PRIO_KEY,
				 config->sched_prio,
				 0, 50);
	if (!is_same_list(oldconfig.devices, oldconfig.num_devices,
			  config->devices, config->num_devices))
		g_key_file_set_string_list(keyfile, DRIVE_GUARD_CONFIG_GROUP,
					   DRIVE_GUARD_CONFIG_DEVICES_KEY,
					   config->devices,
					   config->num_devices);

	result = save_config(keyfile);
	g_key_file_free(keyfile);
	drive_guard_free_config(&oldconfig);

	return result;
}
