/*
 * hp-drive-guard daemon backend
 *
 *   Copyright (c) 2010 Hans Petter Jansson <hpj@novell.com>
 *                      Takashi Iwai <tiwai@suse.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sched.h>
#include <poll.h>

#include "../config.h"
#include "common.h"
#include "daemon-comm.h"

#define FREEFALL_LED_PATH       "/sys/class/leds/hp::hddprotect/brightness"

#define progname	DRIVE_GUARD_CORE_PROG_NAME

static int in_protection;
static int current_timeout;

static int shmfd;
static struct hp_drive_guard_shm *shm;

#define FALSE	0
#define TRUE	1

#define MAX_NUM_DEVS	32
static int blk_fds[MAX_NUM_DEVS];
static int num_devices;

static int set_rt_sched(int prio)
{
	struct sched_param schp;
	int max_prio;

	if (prio <= 0)
		return FALSE;

	memset(&schp, 0, sizeof(schp));
	max_prio = sched_get_priority_max(SCHED_FIFO);
	if (prio > max_prio)
		prio = max_prio;

	schp.sched_priority = prio;
	if (sched_setscheduler(0, SCHED_FIFO, &schp)) {
		fprintf(stderr, "%s: cannot set RT FIFO scheduling\n", progname);
		return FALSE;
	}
	return TRUE;
}

static int write_int(const char *path, int i)
{
	char buf[1024];
	int fd = open(path, O_RDWR);
	int len, result;
	if (fd < 0)
		return FALSE;
	sprintf(buf, "%d", i);
	len = strlen(buf);
	result = write(fd, buf, len);
	close(fd);
	return len == result;
}

static void set_led(int on)
{
	write_int(FREEFALL_LED_PATH, on);
}

static void protect_heads(int seconds)
{
	int i;
	char buf[64];

	sprintf(buf, "%d\n", seconds * 1000);
	for (i = 0; i < num_devices; i++)
		write(blk_fds[i], buf, strlen(buf));
}

static int open_device(const char *block)
{
	char path[128];
	snprintf(path, sizeof(path), "/sys/block/%s/device/unload_heads", block);
	return open(path, O_RDWR);
}

static void freefall_protect_begin(int timeout_s)
{
	protect_heads(timeout_s);
	set_led(1);
}

static void freefall_protect_end(void)
{
	protect_heads(0);
	set_led(0);
}

static void notify_state(int state)
{
	daemon_comm_write(&shm->state, state);
}

static void handle_shock_event(void)
{
	freefall_protect_begin(current_timeout + 1);
	notify_state(DRIVE_GUARD_DRIVE_STATE_PARKED);
	in_protection = current_timeout;
}

static void quit(void)
{
	if (in_protection)
		freefall_protect_end();
	exit(0);
}

static void update_timeout(void)
{
	unsigned int val;

	if (!daemon_comm_read(&shm->cmd, TRUE, &val))
		return;
	if (!val)
		quit();
	if (val <= SHOCK_TIMEOUT_S_MAX)
		current_timeout = val;
}

static void throw_freefall_events(void)
{
	unsigned char tmp[32];
	
	while (read(shm->freefall_fd, tmp, sizeof(tmp)) > 0)
		;
}

static void main_loop(void)
{
	struct pollfd pfds[2];

	pfds[0].fd = shm->freefall_fd;
	pfds[0].events = POLLIN;
	pfds[1].fd = shm->cmd.pipe[P_READER];
	pfds[1].events = POLLIN;
	for (;;) {
		int timeout = -1;
		int ret;

		if (in_protection)
			timeout = in_protection * 1000;
		ret = poll(pfds, 2, timeout);
		if (ret < 0) {
			fprintf(stderr, "%s: select error\n", progname);
			break;
		} else if (ret) {
			if (pfds[0].revents & POLLIN) {
				handle_shock_event();
				throw_freefall_events();
			}
			if (pfds[1].revents & POLLIN)
				update_timeout();
			else if (pfds[1].revents & (POLLERR | POLLHUP))
				quit();
		} else {
			if (in_protection) {
				in_protection = 0;
				freefall_protect_end();
				notify_state(DRIVE_GUARD_DRIVE_STATE_ENABLED);
			}
		}
	}
}

static void server_init(void)
{
	shmfd = shm_open(DRIVE_GUARD_SHM_PATH, O_RDWR, 0);
	if (shmfd < 0) {
		fprintf(stderr, "%s: cannot create SHM\n", progname);
		exit(1);
	}
	shm = mmap(NULL, sizeof(struct hp_drive_guard_shm),
		   PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shm == MAP_FAILED) {
		fprintf(stderr, "%s: cannot mmap SHM\n", progname);
		exit(1);
	}

	current_timeout = SHOCK_TIMEOUT_S_DEFAULT;
	in_protection = 0;
}

static int parse_timeout(const char *arg, int defval)
{
	int timeout = atoi(arg);
	if (timeout < 0 || timeout > SHOCK_TIMEOUT_S_MAX) {
		fprintf(stderr, "%s: invalid timeout seconds %d\n",
			progname, timeout);
		timeout = SHOCK_TIMEOUT_S_DEFAULT;
	}
	return timeout;
}

int main(int argc, char *argv[])
{
	int i, task_prio;

	if (argc < 3) {
		fprintf(stderr, "%s: invalid arguments\n", progname);
		exit(1);
	}

	if (argc - 2 >= MAX_NUM_DEVS) {
		fprintf(stderr, "%s: too many devices\n", progname);
		exit(1);
	}

	task_prio = atoi(argv[1]);
	for (i = 2; i < argc; i++) {
		blk_fds[num_devices] = open_device(argv[i]);
		if (blk_fds[num_devices] >= 0)
			num_devices++;
	}
	if (!num_devices) {
		fprintf(stderr, "%s: no device specified\n", progname);
		exit(1);
	}

	server_init();

	if (task_prio > 0)
		set_rt_sched (task_prio);

	protect_heads (0); /* preallocate buffer */
	mlockall(MCL_FUTURE);

	main_loop();
	return 0;
}
